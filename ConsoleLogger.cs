using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV7_RPPOON
{
    class ConsoleLogger:Logger
    {
        public void Log(SimpleSystemDataProvider provider)
        {
            Console.WriteLine(DateTime.Now + "->CPU LOAD" + provider.CPULoad + " ->Available RAM" + provider.AvailableRAM);
        }

    }
}
