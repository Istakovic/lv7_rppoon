using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace RPP_LV7
{
    class Program
    {
        static void Main(string[] args)
        {
            //Zad1
            NumberSequence arrayExample = new NumberSequence(new double[] { 1.3, 2, 7, 0.1, 12, 58, 2, 9, 222, 4.7 });

            Console.WriteLine("CombSort:\n" + arrayExample.ToString());
            arrayExample.SetSortStrategy(new CombSort());
            arrayExample.Sort();
            Console.WriteLine(arrayExample.ToString());

            NumberSequence arrayExample1 = new NumberSequence(new double[] { 1.3, 2, 7, 0.1, 12, 58, 2, 9, 222, 4.7 });
            Console.WriteLine("BubbleSort:\n" + arrayExample1.ToString());
            arrayExample1.SetSortStrategy(new BubbleSort());
            arrayExample1.Sort();
            Console.WriteLine(arrayExample1.ToString());

            NumberSequence arrayExample2 = new NumberSequence(new double[] { 1.3, 2, 7, 0.1, 12, 58, 2, 9, 222, 4.7 });
            Console.WriteLine("SequentialSort:\n" + arrayExample2.ToString());
            arrayExample2.SetSortStrategy(new SequentialSort());
            arrayExample2.Sort();
            Console.WriteLine(arrayExample2.ToString());
            
            //Zadatak2
            NumberSequence arrayExample3 = new NumberSequence(new double[] { 1.3, 2, 7, 0.1, 12, 58, 2, 9, 222, 4.7 });
            Console.WriteLine("LinearSearch:\n" + arrayExample3.ToString());
            arrayExample3.setSearchStrategy(new LinearSearch());
            Console.WriteLine(arrayExample3.search(4.7));
            
            
            //zadatak3 i 4
            SystemDataProvider dataProvider = new SystemDataProvider();
            ConsoleLogger consoleLogger = new ConsoleLogger();
            dataProvider.Attach(consoleLogger);
            while (true)
            {
                dataProvider.GetAvailableRAM();
                dataProvider.GetCPULoad();  
                System.Threading.Thread.Sleep(1000);
            }

        }
    }
}
